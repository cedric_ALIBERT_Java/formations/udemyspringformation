package fr.cedricalibert.udemySpringFormation;

public class HappyFortuneService implements FortuneService {

	@Override
	public String getFortune() {
		
		return "C'est ton jour de chance !";
	}

}
