package fr.cedricalibert.udemySpringFormation;

public interface Coach {
	public String getDailyWorkout();
	
	public String getDailyFortune();
}
