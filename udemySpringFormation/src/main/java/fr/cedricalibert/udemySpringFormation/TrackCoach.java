package fr.cedricalibert.udemySpringFormation;

public class TrackCoach implements Coach{
	
	//define a private field for dependency
	private FortuneService fortuneService;
	
	//define a no-args constructor
	public TrackCoach() {
		
	}
	
	//define a constructor
	public TrackCoach(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}
	
	@Override
	public String getDailyWorkout() {
		return "Cours 5 km !";
	}
	
	@Override
	public String getDailyFortune() {
		// use my fortune service to get a fortune
		return "Just Do It: " + this.fortuneService.getFortune();
	}
	
	//add an init method
	public void doMyStartupStuff() {
		System.out.println("TrackCoach: Inside method doMyStartupStuff");
	}
	
	//add a destroy method
	public void doMyCleanUpStuff() {
		System.out.println("TrackCoach: Inside method doMyCleanUpStuff");
	}
}
