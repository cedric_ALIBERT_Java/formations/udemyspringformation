package fr.cedricalibert.udemySpringFormation;

public class BaseballCoach implements Coach{
	
	//define a private field for dependency
	private FortuneService fortuneService;
	
	//define a constructor
	public BaseballCoach(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}
	
	@Override
	public String getDailyWorkout() {
		return "Cours 30 minutes !";
	}
	
	@Override
	public String getDailyFortune() {
		// use my fortune service to get a fortune
		return this.fortuneService.getFortune();
	}
}
